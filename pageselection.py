from collections import OrderedDict
max_weight = 0 
global_word_list = {} 
global_list_of_pages = {} 
global_list_of_queries = [] 
n=5
class factory:
   def __init__(self):
        self.name = ""
        self.word_list = {}
        self.count = 8
        self.listOfWords =[]
        self.sum = 0

    def getName(self):
        return self.name

    def getWordList(self):
        return self.word_list

    def setName(self, name):
        self.name = name

    def setCount(self,count):
        self.count = count

    def setList(self,words):
        self.listOfWords = words

    def addToList(self):
        for word in self.listOfWords:
            if word not in self.word_list:
                self.word_list[word] = self.count - 1
                self.count -= 1

def add_to_global(words, pageName):
    for word in words:
        if word in global_word_list:
            global_word_list[word].append(pageName)
        else:
            global_word_list[word] = [pageName]

def create(value,i,type):
    page = factory()
    words = value.split()
    findMax(len(words))
    page.setList(words)
    if type == 'p':
        page.setName("P" + str(i + 1))
        add_to_global(words, page.getName())
        global_list_of_pages[page.getName()] = page
    else:
        page.setName("Q" + str(i + 1))
        global_list_of_queries.append(page)

def findMax(number):
    global max_weight
    if max_weight < number:
        max_weight = number
def searchMain(): 
    for query in global_list_of_queries:
        visited =[]
        d ={}
        for word in query.getWordList():
            if word in global_word_list:
                for page in global_word_list[word]:
                    if page not in visited:
                        visited.append(page)
                        sop = sumOfProducts(query,global_list_of_pages[page])
                        d[page] = sop
        d = OrderedDict(sorted(d.items(), key=lambda x: (-x[1], (x[0][0], int(x[0][1:])))))
        forPrint(query.getName(),d)

def sumOfProducts(query,page):   
    sop = 0
    for qword in query.getWordList():
        if qword in page.getWordList():
            sop = sop + query.getWordList()[qword] * page.getWordList()[qword]
    return sop        
 def forPrint(name,Dict):
    print(name,':',end=" ")
    i = 0
    for key in Dict.keys():
        if i < n:
            print(key, end=" ")
            i += 1
        if i >= n:
            break
    print("")

def assignWeights():
    for key,page in global_list_of_pages.items():
        page.setCount(max_weight)
        page.addToList()
    for query in global_list_of_queries:
        query.setCount(max_weight)
        query.addToList()
        
def readFromFile():
        print("Input read from input.txt file : ")
        f = open('input.txt','r')
        page_index =0
        query_index = 0
        for line in f:
            print(line,end="")
            if line[0] == 'P':
                create(line[1:],page_index,'p')
                page_index += 1

            if line[0] =='Q':
                create(line[1:],query_index,  6'q')
                query_index += 1
        print("\n")

if __name__ == '__main__':
    readFromFile()
    assignWeights()
    searchMain()

